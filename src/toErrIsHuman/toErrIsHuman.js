const test = require('tape');

const feedCat = require(process.argv[2]);

test('Can feed cat food', (t) => {
  try {
    t.equal(feedCat('food'), 'yum');
  } catch (error) {
    t.fail('Cats should be able to eat cat food');
  }
  t.end();
});

test('Can not feed cat chocolate', (t) => {
  try {
    feedCat('chocolate');
    t.fail('Cats can\'t eat chocolate');
  } catch (error) {
    t.pass();
  }
  t.end();
});
