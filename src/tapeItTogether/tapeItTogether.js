const test = require('tape');

const fancify = require(process.argv[2]);

test('fancify wraps string in ~*~', (t) => {
  t.equal(fancify('Hello'), '~*~Hello~*~', 'Input Hello should output ~*~Hello~*~');
  t.end();
});

test('fancify converts string to all caps', (t) => {
  t.equal(fancify('Hello', true), '~*~HELLO~*~', 'Input Hello should output ~*~HELLO~*~');
  t.end();
});

test('fancify change middle character', (t) => {
  t.equal(fancify('Hello', false, '!'), '~!~Hello~!~', 'Input Hello should output ~!~Hello~!~');
  t.end();
});
