const test = require('tape');

const repeatCallBack = require(process.argv[2]);

test('Repeat CallBack 5 times', (t) => {
  const count = 5;

  const callBack = () => { t.pass('I was called'); };

  t.plan(count);
  repeatCallBack(count, callBack);
});
